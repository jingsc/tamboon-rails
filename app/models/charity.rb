class Charity < ActiveRecord::Base
  validates :name, presence: true

  def credit_amount(amount)
    with_lock do
      self.total += amount
      self.save!
    end
  end

  def self.random
    charity_at = Random.rand(1..self.count-1)
    return self.offset(charity_at).first()
  end
end
