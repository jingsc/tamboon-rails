class WebsiteController < ApplicationController

  def index
    @token = nil
  end

  def donate
    if donatable?
      @charity.credit_amount(@charge.amount)
      flash.notice = t(".success")
      return redirect_to root_path if not random_donation?
    else
      flash.now.alert = t(".failure")
    end

    return render :index
  end

  private

  MIN_DONATION_AMOUNT = 2_000 # satang

  # TODO chould be enhance with accumuated error messages
  # TODO chould be support multi currency
  def donatable?
    # determined by
    # 1. valid charity
    # 2. valid credit card and amount
    
    @charity = nil
    if params[:charity].present?
      if random_donation?
        @charity = Charity.random()
      else
        cid = params[:charity].to_i
        @charity = Charity.find(cid) rescue nil
      end
    end

    @charge = nil # charge combined with amount and card
    if params[:amount].present? and params[:omise_token].present?
      amount = params[:amount].to_f
      satang_amount = (amount * 100).to_i
      if satang_amount > MIN_DONATION_AMOUNT and not @charity.nil?
        # create omise donation charge
        @charge = Omise::Charge.create({
          amount: satang_amount,
          currency: "THB",
          card: params[:omise_token],
          description: "Donation to #{@charity.name} [#{@charity.id}]",
        })
      else
        @token = Omise::Token.retrieve(params[:omise_token])
      end
    end

    # check all conditions
    if @charity.nil? or @charge.nil? or not @charge.paid
      return false
    else
      return true
    end
  end


  def random_donation?()
    return params[:charity].downcase == "random"
  end
end
